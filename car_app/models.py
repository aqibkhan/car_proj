from django.db import models

from django.utils import timezone


class ParkingSpace(models.Model):
    created_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)

    space_id = models.PositiveIntegerField()
    is_available = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Parking Space'
        verbose_name_plural = 'Parking Spaces'
        db_table = 'parking_space'

    def __str__(self):
        return '{}: <{}>'.format(self.__class__.__name__, self.space_id)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        super().save(*args, **kwargs)


class Car(models.Model):
    created_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)

    number_plate = models.CharField(max_length=20)
    in_time = models.DateTimeField(null=True, blank=True)
    out_time = models.DateTimeField(null=True, blank=True)
    parking_space = models.ForeignKey(
        ParkingSpace, on_delete=models.CASCADE,
        related_name='parking_spaces', related_query_name='parking_space')

    class Meta:
        verbose_name = 'Car'
        verbose_name_plural = 'Cars'
        db_table = 'car'

    def __str__(self):
        return '{}: <{}>'.format(self.__class__.__name__, self.number_plate)

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        super().save(*args, **kwargs)
