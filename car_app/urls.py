from django.urls import path

from .views import *


urlpatterns = [
    # Parking space list API (query params (space_type=allotted/empty/all) required)
    path('parking-spaces/', ParkingSpaceList.as_view(), name='car-parking-space-list'),
    # Parking space Detail API
    path('parking-spaces/<int:pk>/', ParkingSpaceDetail.as_view(), name='car-parking-space-detail'),
    # Car checkin API (reserve parking space)
    path('parking-spaces/<int:psk>/checkin/', CarCheckInView.as_view(), name='car-checkin'),
    # Car checkout API (free parking space)
    path('parking-spaces/<int:psk>/checkout/<int:pk>/', CarCheckOutView.as_view(), name='car-checkout'),
]