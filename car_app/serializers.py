from rest_framework import serializers

from .models import Car, ParkingSpace


class CarSerializer(serializers.ModelSerializer):

    class Meta:
        model = Car
        exclude = ('created_at', 'updated_at')


class ParkingSpaceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParkingSpace
        exclude = ('created_at', 'updated_at')
