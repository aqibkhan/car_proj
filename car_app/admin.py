from django.contrib import admin

from .models import Car, ParkingSpace


class CarModelAdmin(admin.ModelAdmin):
    list_display = ('number_plate', 'in_time', 'out_time', 'parking_space')
    list_filter = ('parking_space', 'in_time', 'out_time')
    search_fields = ('number_plate', 'parking_space__space_id', )


admin.site.register(Car, CarModelAdmin)


class ParkingSpaceModelAdmin(admin.ModelAdmin):
    list_display = ('space_id', 'is_available')
    list_filter = ('is_available', )
    search_fields = ('space_id',)


admin.site.register(ParkingSpace, ParkingSpaceModelAdmin)
