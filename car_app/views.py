from django.utils import timezone
from django.shortcuts import get_object_or_404

from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import exceptions
from rest_framework import permissions

from .models import Car, ParkingSpace
from .serializers import CarSerializer, ParkingSpaceSerializer


class ParkingSpaceList(ListAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer
    permission_classes = (permissions.AllowAny,)
    pagination_class = None

    def get(self, request, *args, **kwargs):

        availability = self.request.query_params.get('availability', None)

        if not availability:
            raise exceptions.ValidationError({'detail': 'allotted/empty space type is required'})

        elif availability == 'allotted':
            queryset = ParkingSpace.objects.filter(is_available=False).order_by('space_id')
        elif availability == 'empty':
            queryset = ParkingSpace.objects.filter(is_available=True).order_by('space_id')
        elif availability == 'all':
            queryset = ParkingSpace.objects.all().order_by('space_id')
        else:
            raise exceptions.ValidationError({'detail': 'Pass valid parameter'})

        data = self.serializer_class(queryset, many=True).data
        data = {
            'count': queryset.count(),
            'result': data
        }
        return Response(data, status=status.HTTP_200_OK)


class ParkingSpaceDetail(RetrieveAPIView):
    queryset = ParkingSpace.objects.all()
    serializer_class = ParkingSpaceSerializer
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        parked_cars = Car.objects.filter(
            parking_space=instance, in_time__isnull=False, out_time__isnull=True, parking_space__is_available=False)
        if parked_cars.exists():
            parked_car = parked_cars.first()
            car_data = CarSerializer(parked_car).data
        else:
            car_data = None
        data = self.serializer_class(instance).data
        data['car_data'] = car_data
        return Response(data, status=status.HTTP_200_OK)


class CarCheckInView(CreateAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        # make_request_mutable(request)
        if not ('number_plate' in self.request.data and self.request.data['number_plate']):
            raise exceptions.ValidationError({'number_plate': ['Number plate is required']})

        if Car.objects.filter(number_plate=self.request.data['number_plate'], in_time__isnull=False, out_time__isnull=True).exists():
            raise exceptions.ValidationError({
                'detail': f"Car with plate number '{self.request.data['number_plate']}' is already in parking area."})

        parking_space = get_object_or_404(ParkingSpace, id=self.kwargs['psk'])

        if not parking_space.is_available:
            raise exceptions.ValidationError(
                {'detail': f'Parking space {parking_space.space_id} is not available to park'})
        self.request.data['parking_space'] = self.kwargs['psk']
        self.request.data['in_time'] = timezone.now()
        car_serializer = CarSerializer(data=self.request.data)
        car_serializer.is_valid(raise_exception=True)
        car_serializer.save()

        parking_space.is_available = False
        parking_space.save()

        data = car_serializer.data
        data.update({"detail": 'You have parked at {} parking space'.format(parking_space.space_id)})

        return Response(data, status=status.HTTP_201_CREATED)


class CarCheckOutView(RetrieveAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        parking_space = get_object_or_404(ParkingSpace, id=self.kwargs['psk'])
        obj = self.get_object()
        try:
            instance = Car.objects.filter(
                parking_space=parking_space, parking_space__is_available=False,
                number_plate=obj.number_plate, in_time__isnull=False, out_time__isnull=True
            ).latest('updated_at')
        except:
            raise exceptions.ValidationError(
                {'detail': f'Car {obj.number_plate} is not parked at {parking_space.space_id}. '
                f'Please find the correct space and checkout'}
            )

        if parking_space.is_available:
            raise exceptions.ValidationError({'detail': 'You can\'t checkout car. Your car is not parked here.'})

        if instance.in_time:
            if not instance.out_time:
                instance.out_time = timezone.now()
                parking_space.is_available = True
                instance.save()
                parking_space.save()
            else:
                raise exceptions.ValidationError(
                    {'detail': f"Car {instance.number_plate} is already out from parking {instance.parking_space.space_id} area"}
                )
        else:
            raise exceptions.ValidationError({'detail': f"Car {instance.number_plate} is not in the parking area"})

        return Response({'detail': f'Car {instance.number_plate} checked out successfully'})
