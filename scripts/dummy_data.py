from car_app.models import ParkingSpace

from django.contrib.auth.models import User


def create_parking_space(num):
    for i in range(1, num + 1):
        space_id = 1000 + i
        data = {
            'space_id': space_id
        }
        ParkingSpace.objects.get_or_create(**data)


def create_superuser():
    data = {
        'username': 'admin',
        'email': 'admin@gmail.com',
        'is_superuser': True,
        'is_active': True,
        'is_staff': True
    }
    user, is_created = User.objects.get_or_create(**data)
    user.set_password('qwertyuiop')
    user.save()


def run():
    num = 20
    print('Parking slot data creating')
    create_parking_space(num)
    print('Parking slot data created')
    print('Super user creating')
    create_superuser()
    print('Super user created')
